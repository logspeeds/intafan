import { StyleSheet, Dimensions } from "react-native";

const widths = Dimensions.get("screen").width;

const style = StyleSheet.create({
  conteiner: {
    flexGrow: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  inputs: {
    width: widths * 0.8,
    textAlign: "center",
    marginTop: 40,
    fontSize: 30,
  },
  buttonView: {
    alignItems: "center",
    marginBottom: 50,
  },
});
export default style;
