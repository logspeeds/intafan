import React, { Fragment, useState } from "react";
import { TextInput, Button, View } from "react-native";
import SendInformarion from "../../api/login";

import style from "./style";

const Login = () => {
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");

  const sendInformarion = () => {
    SendInformarion(user, password);
  };

  return (
    <Fragment>
      <View style={style.conteiner}>
        <TextInput
          style={style.inputs}
          placeholder="Usuário"
          onChangeText={(text) => setUser(text)}
        />
        <TextInput
          style={style.inputs}
          placeholder="Senha"
          secureTextEntry={true}
          onChangeText={(text) => setPassword(text)}
        />
      </View>
      <Button
        style={style.buttonView}
        title="Entrar"
        onPress={sendInformarion}
      />
    </Fragment>
  );
};
export default Login;
