import React, { Fragment, useState, useEffect } from "react";

import { ScrollView, FlatList, StatusBar, Platform } from "react-native";

/*Concentramos todos os imports em um único view e passamos o que for
necessário para os componentes por "props" (stateless components) - Rafael Penteado*/
import { addNewComment } from "../../api/comment";
import { imgLike, likePhoto } from "../../api/likes";

import readPhotos from "../../api/feed";
import { Comments } from "../../Components/Comments";
import { Header } from "../../Components/Header";
import { Photo } from "../../Components/Photo";

const Feed = () => {
  const [info, setInfo] = useState([]);
  useEffect(() => {
    readPhotos(setInfo);
    // console.warn(addNewComment);
  }, []);

  /*Existe uma diferença de altura pertiente na StatusBar do
  android e ios - Rafael Penteado*/
  let height = 0;

  if (Platform.OS === "ios") {
    height = 35;
  }

  return (
    <ScrollView style={{ marginTop: height }}>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <FlatList
        data={info}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <Fragment>
            <Header userName={item.userName} userURL={item.userURL} />
            <Photo
              urlPhoto={item.url}
              description={item.description}
              likeCount={item.likes}
              imgLike={imgLike}
              likePhoto={likePhoto}
            />
            <Comments
              comments={item.comentarios}
              addNewComments={addNewComment}
            />
          </Fragment>
        )}
      />
    </ScrollView>
  );
};

export default Feed;
