import React, { Fragment, useState } from "react";
import { Text, Image, TouchableOpacity, View } from "react-native";
import style from "./style";

const Photo = ({ urlPhoto, description, likeCount, imgLike, likePhoto }) => {
  const [liked, setLiked] = useState(false);
  const [likes, setLikes] = useState(likeCount);

  const clickedPhoto = () => {
    const [newStateLike, count] = likePhoto(liked, likes);
    setLiked(newStateLike);
    setLikes(count);
  };

  return (
    <Fragment>
      <Image source={{ uri: urlPhoto }} style={style.image} />
      <Text>{description}</Text>
      <View style={style.viewLike}>
        <TouchableOpacity onPress={clickedPhoto}>
          <Image source={imgLike(liked)} style={style.like} />
        </TouchableOpacity>
        <Text> {likes} curtidas</Text>
      </View>
    </Fragment>
  );
};

export default Photo;
