import { StyleSheet } from "react-native";

const style = StyleSheet.create({
  photoUser: {
    width: 50,
    height: 50,
    margin: 10,
    borderRadius: 30,
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default style;
