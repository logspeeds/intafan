import React, { Fragment } from "react";

import { View, Text, Image } from "react-native";

import style from "./style.js";

const Header = ({ userName, userURL }) => {
  return (
    <View style={style.header}>
      <Image source={{ uri: userURL }} style={style.photoUser} />
      <Text>{userName}</Text>
    </View>
  );
};

export default Header;
