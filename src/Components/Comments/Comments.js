import React, { Fragment, useState } from "react";
import {
  Text,
  FlatList,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from "react-native";
import style from "./style";

const Comments = ({ comments, addNewComments }) => {
  const [stateComment, setComment] = useState(comments);

  const addComment = () => {
    inputField.clear();
    const newComment = addNewComments(inputContentField, "logspeeds");
    setComment([...stateComment, newComment]);
  };

  let inputField;
  let inputContentField = "";

  return (
    <Fragment>
      <FlatList
        data={stateComment}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <View style={style.inline}>
            <Text>{item.userName} comentou: </Text>
            <Text>{item.text}</Text>
          </View>
        )}
      />

      <View style={style.inline}>
        <TextInput
          ref={(TextInput) => (inputField = TextInput)}
          onChangeText={(text) => (inputContentField = text)}
          placeholder={"Deixe seu comentário..."}
          style={{ flex: 1 }}
        />
        <TouchableOpacity onPress={addComment}>
          <Image
            source={require("../../../res/img/send.png")}
            style={style.imgSend}
          />
        </TouchableOpacity>
      </View>
    </Fragment>
  );
};

export default Comments;
