const imgLike = (likeCount) => {
  if (likeCount > 0) {
    return require("../../res/img/s2-checked.png");
  } else {
    return require("../../res/img/s2.png");
  }
};

const likePhoto = (liked, likes) => {
  let count = likes;
  if (liked) {
    count--;
  } else {
    count++;
  }
  return [!liked, count];
};

export { imgLike, likePhoto };
