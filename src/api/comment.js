const addNewComment = (comment, user) => {
  const newcomment = {
    date: Date.now(),
    text: comment,
    userName: user,
  };
  return newcomment;
};

export { addNewComment };
