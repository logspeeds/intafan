import { Platform } from "react-native";

/*Diferenciar durante a virtualização se estamos
em um Android ou iOS - Rafael Penteado */

const readPhotos = async (setInfo) => {
  let url = "10.0.2.2";
  if (Platform.OS === "ios") {
    url = "localhost";
  }

  const photosHTTP = await fetch(`http://${url}:3030/feed`);
  const photosJson = await photosHTTP.json();
  setInfo(photosJson);
};

export default readPhotos;
